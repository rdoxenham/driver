## Driver Installation for SLES 15 SP4+ and SLE Micro 5.3+

These instructions will walk you through the installation and utilisation of the NVIDIA drivers for use within Kubernetes on SUSE Linux Enterprise Server 15 SP4 (SLES), and SUSE Linux Enterprise Micro 5.3+ (SLE Micro) based platforms. The instructions assume some familiarity with how NVIDIA drivers are built, and how to navigate container image building with podman.

> **NOTE**: Please make sure that your target systems are registered via `SUSEConnect` to enable package access when building the container images. For more information, please see [here](https://www.suse.com/support/kb/doc/?id=000018564).

1. First, clone this repository locally and enter the `sle15` directory:
   ```bash
   % git clone https://gitlab.com/nvidia/container-images/driver
   (...)
   
   % cd driver/sle15
   ```

2. Now you'll need to find an appropriate NVIDIA driver version to download for your target GPU, and this version will be determined by both the type of card that you're using (e.g. consumer-grade or datacentre-grade) and your usecase (e.g. [vGPU](https://www.nvidia.com/en-gb/data-center/virtual-solutions/) or [passthrough](https://docs.nvidia.com/grid/5.0/grid-vgpu-user-guide/index.html#using-gpu-pass-through)). See below for the two primary options:

   1. If you're wanting to utilise **vGPU** capabilities on a datacentre-grade card, you'll need to sign in to your [NVIDIA Licensing Portal](https://ui.licensing.nvidia.com/), navigate to "**Software Downloads**" and download both the latest "**vGPU Driver Catalog**" file (*vgpuDriverCatalog.yaml*) , and the latest "**Linux KVM**" package (which will have a "*NVIDIA-GRID-Linux-KVM-<version>.zip*" file).

      Once you've got these files downloaded, unzip the NVIDIA GRID driver zip file and copy the guest GRID driver file to the "drivers/" subdirectory in the repository. The following instructions assume driver version "525.105.17":

      ~~~bash
      % unzip /path/to/downloads/NVIDIA-GRID-Linux-KVM-525.105.14-525.105.17-528.89.zip
      % mv Guest_Drivers/NVIDIA-Linux-x86_64-525.105.17-grid.run drivers/
      % mv /path/to/downloads/vgpuDriverCatalog.yaml drivers/
      ~~~

   2. If you're wanting to use consumer-grade cards (that only support passthrough, not vGPU), you can download the latest supported driver from the standard NVIDIA Official Drivers, found [here](https://www.nvidia.com/download/index.aspx). Simply download the file and place it into the drivers directory (again, we assume *530.30.02* as the driver version):
      ~~~bash
      % wget -P drivers/ https://us.download.nvidia.com/XFree86/Linux-x86_64/530.30.02/NVIDIA-Linux-x86_64-530.30.02.run
      ~~~

      >  **NOTE**: If you're ***not*** using vGPU and are using the full passthrough (e.g. for timeslicing) you will need to make sure that there's equivalent nvidia-fabricmanager and libnvidia-nscq packages for your chosen NVIDIA driver version available [here](https://developer.download.nvidia.com/compute/cuda/repos/sles15/x86_64/). If your chosen version doesn't have the equivalent packages, your build will fail.

3. Now you're ready to build the driver container. For this, you'll need to specify a number of parameters to the build, namely:

   * A build tag - useful for when you're uploading to your image registry for use with Kubernetes (specified with "**-t**")
   * The version of SLES that you're using, noting that SLE Micro is based on SLES, e.g. "**15.4**" would also be suitable for SLE Micro 5.3/5.4.
   * The type of driver we're building for, i.e. "**vgpu**" or "**passthrough**".
   * The architecture that you're building for, i.e. "**x86_64**" or "**aarch64**".
   * The driver version that you're building, taken from the version you previously downloaded, e.g. "**525.105.17**". Noting that if you're building for vGPU you'll need to append "**-grid**" to your driver version.
   * The CUDA version you wish to build into your NVIDIA driver image, e.g. "**12.1.1**".


   When ready, issue the following command with your specific requirements, the example below uses a fairly new vGPU driver configuration on x86_64:
   ~~~bash
   % podman build -t my.container-repo.com/nvidia-sle15:525.105.17-grid \
       --build-arg SLES_VERSION="15.4" \
       --build-arg DRIVER_TYPE="vgpu" \
       --build-arg DRIVER_ARCH="x86_64" \
       --build-arg DRIVER_VERSION="525.105.17-grid" \
       --build-arg CUDA_VERSION="12.1.1" \
       --build-arg SLES_VERSION="15.4" \
       .
   
   (...)
   Successfully tagged my.container-repo.com/nvidia-sle15:525.105.17-grid
   88578c5dc0c3e68ff1effc151405c2df06b5be366a002004cf200f16f2593cea
   ~~~

   > **NOTE**: You can substitute the `podman` commands with `docker` if required.

   Another example for non-vGPU, i.e. "passthrough" driver builds:

   ```bash
   % podman build -t my.container-repo.com/nvidia-sle15:530.30.02 \
       --build-arg SLES_VERSION="15.4" \
       --build-arg DRIVER_TYPE="passthrough" \
       --build-arg DRIVER_ARCH="x86_64" \
       --build-arg DRIVER_VERSION="530.30.02" \
       --build-arg CUDA_VERSION="12.1.1" \
       --build-arg SLES_VERSION="15.4" \
       .
   
   (...)
   Successfully tagged my.container-repo.com/nvidia-sle15:530.30.02
   1823919a9d3b90c3d995aae7f7b3cb04cd3886891e49a983a508f90d33482a6f
   ```

4. Once you've got your NVIDIA driver built, you can test to ensure that it's functioning as expected on your target platform, and verify that it's able to build and load the NVIDIA driver by manually starting the nvidia-driver container you just built:
   ~~~bash
   % podman run --name nvidia-driver -d --privileged --pid=host \
   		-v /run/nvidia:/run/nvidia:shared -v /var/log:/var/log \
   		--restart=unless-stopped my.container-repo.com/nvidia-sle15:525.105.17-grid
   
   % podman logs -f nvidia-driver
   Creating directory NVIDIA-Linux-x86_64-525.105.17-grid
   Verifying archive integrity... OK
   Uncompressing NVIDIA Accelerated Graphics Driver for Linux-x86_64 525.105.17...
   (...)
   ~~~

   > **NOTE**: You may have to temporarily create the /run/nvidia directory with `mkdir -p /run/nvidia`.

   At this point, the system should download the necessary packages from the SUSE package repositories to build the kernel modules locally. You may see some warnings as things progress, but at the end you should see a "**Done, now waiting for signal**" message (Ctrl-C to exit).

   Now let's check that the NVIDIA drivers have been loaded successfully, and that `nvidia-smi` can view your device:
   ~~~bash
   % lsmod | grep nvidia
   nvidia_modeset       1245184  0
   nvidia_uvm           1339392  0
   nvidia              56516608  17 nvidia_uvm,nvidia_modeset
   drm                   634880  4 drm_kms_helper,nvidia,mgag200
   
   % podman exec -it nvidia-driver nvidia-smi
   Tue Jun 13 20:23:46 2023
   +-----------------------------------------------------------------------------+
   | NVIDIA-SMI 525.105.17   Driver Version: 525.105.17   CUDA Version: 12.0     |
   |-------------------------------+----------------------+----------------------+
   | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
   | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
   |                               |                      |               MIG M. |
   |===============================+======================+======================|
   |   0  NVIDIA A100 80G...  On   | 00000000:17:00.0 Off |                    0 |
   | N/A   36C    P0    43W / 300W |      0MiB / 81920MiB |      0%      Default |
   |                               |                      |             Disabled |
   +-------------------------------+----------------------+----------------------+
   
   +-----------------------------------------------------------------------------+
   | Processes:                                                                  |
   |  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
   |        ID   ID                                                   Usage      |
   |=============================================================================|
   |  No running processes found                                                 |
   +-----------------------------------------------------------------------------+
   ~~~

5. Now that we've successfully proven the image can be built, we need to clean up and unload the modules (which will happen automatically when we kill the nvidia-driver pod):

   ~~~bash
   % podman rm -f nvidia-driver
   
   % lsmod | grep nvidia | wc -l
   0
   ~~~

6. At this stage, you're ready to push your image to your local registry so it can be referenced in the NVIDIA GPU Operator Helm chart:
   ~~~bash
   % podman push my.container-repo.com/nvidia-sle15:525.105.17-grid
   (...)
   ~~~

7. Finally, you can proceed with the installation of the GPU Operator, instructions can be found [here](https://docs.nvidia.com/datacenter/cloud-native/kubernetes/anthos-guide.html#install-gpu-operator), and you may have to configure the licence information if you're running GRID/vGPU (instructions [here](https://docs.nvidia.com/datacenter/cloud-native/gpu-operator/install-gpu-operator-vgpu.html#configure-the-cluster-with-the-vgpu-license-information-and-the-driver-container-image)).
   

> **WARNING**: It's important to recognise that whilst these instructions may function for a wide variety of cards and use-cases, official NVIDIA support may not be available for them all. You can find comprehensive support information [here](https://docs.nvidia.com/datacenter/cloud-native/gpu-operator/platform-support.html#supported-deployment-options-hypervisors-and-nvidia-vgpu-based-products), or alternatively, please enquire with your NVIDIA representative.
